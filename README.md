# Persistent identifiers of textual segments as a scholarly problem



[Neven Jovanović](orcid.org/0000-0002-9119-399X), Department of Classical Philology, Faculty of Humanities and Social Sciences, University of Zagreb

## What?

[The talk](jovanovic_rda_talk.md)

[The slides](http://croala.ffzg.unizg.hr/rda-citation-research)

[Slides as PDF](jovanovic_rda_slides_Persistent_identifiers.pdf)

## How?

The slides for the presentation are made with the [reveal.js](https://github.com/hakimel/reveal.js/) HTML presentation framework.

# Licence

[CC-BY](LICENSE.md)
