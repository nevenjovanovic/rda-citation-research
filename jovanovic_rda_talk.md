# Persistent identifiers of textual segments as a scholarly problem

The 10-minutes presentation for the RDA meets Croatian researchers event, 24 October 2017, Zagreb, Croatia

Neven Jovanović, Faculty of Humanities and Social Sciences, University of Zagreb, Croatia

I speak in the name of a community of people working with texts, in Croatia and beyond it. [slide 1](http://croala.ffzg.unizg.hr/rda-citation-research/#/1)

For us, citations are data, or, to be more precise, addresses at which we find data that we need. [2](http://croala.ffzg.unizg.hr/rda-citation-research/#/2)

To find it successfully, we need to be able to point exactly, not only to a text, but to one segment of it -- sometimes, to one segment of a specific version of it. Over the centuries, the physical world and the world of writing and print has developed standards for this precise pointing to segments: for example, Mark 2:15, or "Plato in The Republic criticizes the division of labor based on sex (454d-456b)." [3](http://croala.ffzg.unizg.hr/rda-citation-research/#/3)

In the digital medium, however, we don't cite just for documentation -- we cite the segment to be able to call it up, to do something with it: to transform it into an entry in a database or to add grammatical annotations. And we want others to do something with that segment too -- this is sometimes called impact.

Some of our colleagues have developed a technological protocol to achieve this fine-grained pointing to textual segments and to different versions of the same text. The standard is called CTS, Canonical Text Services (CTS) Protocol. [4](http://croala.ffzg.unizg.hr/rda-citation-research/#/4)

What we are exploring now is a standard approach to automatically link CTS URNs with APIs which can actually resolve them, [5](http://croala.ffzg.unizg.hr/rda-citation-research/#/5) to enable, for example, the following use cases [6](http://croala.ffzg.unizg.hr/rda-citation-research/#/6), [7](http://croala.ffzg.unizg.hr/rda-citation-research/#/7):

+ US 1 As a user I want a CTS Work URN (with a passage reference) to resolve to a list of URLs serving editions and translations of this work.
+ US 4 As a user I want a CTS Edition/Translation URN with a passage reference to resolve to a list of URLs at which I can retrieve that passage.
+ US 5 As a user I want a CTS Edition/Translation URN which has been retired/replaced to resolve to a list of URLs for the replacement edition/translation.
+ US 7 As a publisher I want to be able to publish a edition or translation URN for a text that in a namespace that is managed by another publisher.
+ US 10 As a publisher I want to be able to provide a single URL at which all CTS URN identified texts I publish are available.
+ US 11 As a publisher I want to be able to provide URLs at which subsets of CTS URN identified texts I publish are available.

Now you are probably thinking "but this has already been solved several times over! We have a number of reliable and trustworthy persistent identifiers: Handle, DOI, ARK, URN, URN:NBN, PURL." [8](http://croala.ffzg.unizg.hr/rda-citation-research/#/8)

You are correct. 

But, to my knowledge, none of these identifiers has the semantic power to express that a segment belongs to a larger whole in the way that, for example, the URL http://www.srce.unizg.hr/vijesti/research-data-alliance-rda-sastaje-se-s-hrvatskim-istrazivacima/objav2017-06-19 [9](http://croala.ffzg.unizg.hr/rda-citation-research/#/9) suggests -- to somebody who speaks Croatian -- that there is an article about RDA which belongs to the News section of the SRCE portal. 

The CTS protocol enables such semantic expression both for humans and for machines [10](http://croala.ffzg.unizg.hr/rda-citation-research/#/10) -- and we, as humans, need that to be able to program the machines.

What my colleagues are proposing to RDA is to combine the Handle / DOI system with the CTS protocol, and to add a governance structure for it, so that a consumer of a CTS URN can get to its text segment without having to know where and how a publisher resolves the URN. [11](http://croala.ffzg.unizg.hr/rda-citation-research/#/11) 

Or, as they say, "a standard approach which allows for distributed publishing but centralized management of the resolution". [12](http://croala.ffzg.unizg.hr/rda-citation-research/#/12)

Now, where could Croatia fit into this? The proposed governance model sees several roles in this ecosystem: [13](http://croala.ffzg.unizg.hr/rda-citation-research/#/13)

+ Centralized Handle System Provider (CHSP) - one or more organizations assuming responsiblity for registering and administering Handle prefixes for CTS Namespaces
+ Participating CTS Text Publisher (PCTP) - a publisher of CTS URN identified texts who wants their text URNs to be globally resolved by the Handle System
+ hdl.handle.net provider (HDL) - the provider of the global hdl.handle.net proxy service
+ Non-Participating CTS Text Publisher (NPCTP) - a publisher of CTS URN identified texts who does not want to participate in the centralized solution but wants to publish a Handle for their text

I know that we in Croatia have projects interested in publishing CTS references to the texts they are researching. I personally am involved in two such projects and several more are underway, more of them across Europe. 

I think that it would be good to have a discussion about Croatian research infrastructure taking part in some of the other proposed roles. [14](http://croala.ffzg.unizg.hr/rda-citation-research/#/14)

Thank you.



